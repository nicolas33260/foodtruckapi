<?php
/**
 * Created by PhpStorm.
 * User: Nicolas
 * Date: 2019-02-06
 * Time: 13:17.
 */

namespace App\Tests\Controller;

use App\Entity\FoodTruck;
use Symfony\Component\HttpFoundation\Response;
use App\Tests\Base\AbstractTest;

class BookingControllerTest extends AbstractTest
{
    public function testListSuccess()
    {
        $client = static::createClient();
        $client->request('GET', 'api/v1/bookings?date=06-02-2019');

        $list = json_decode($client->getResponse()->getContent(), true);

        $this->assertEquals(200, $client->getResponse()->getStatusCode());
        $this->assertArrayHasKey('bookings', $list);
        $this->assertArrayHasKey('bookingStillAvailable', $list);
        $this->assertArrayHasKey('bookingAvailableForThisDay', $list);
    }

    public function testListFailed()
    {
        $client = static::createClient();
        $client->request('GET', 'api/v1/bookings?date=32/02/19');
        $this->assertEquals(400, $client->getResponse()->getStatusCode());
    }

    public function testCreate()
    {
        $foodTruck = self::$manager
            ->getRepository(FoodTruck::class)
            ->findOneBy(['name' => 'Foo1']);

        $booking = [
            'booking' => [
                'date' => '15-02-2019',
                'foodTruck' => $foodTruck->getId(),
            ],
        ];

        $client = static::createClient();
        $client->request(
            'POST', 'api/v1/booking', [], [],
            ['CONTENT_TYPE' => 'application/json'],
            json_encode($booking)
        );

        $response = json_decode($client->getResponse()->getContent(), true);

        $this->assertEquals(Response::HTTP_CREATED, $client->getResponse()->getStatusCode());
        $this->assertArrayHasKey('id', $response);
        $this->assertArrayHasKey('date', $response);
        $this->assertArrayHasKey('foodTruck', $response);
    }
}
