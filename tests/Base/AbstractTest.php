<?php
/**
 * Created by PhpStorm.
 * User: Nicolas
 * Date: 2019-01-03
 * Time: 13:47.
 */

namespace App\Tests\Base;

use App\Fixtures\FoodTruckFixture;
use Doctrine\Common\DataFixtures\Executor\ORMExecutor;
use Doctrine\Common\DataFixtures\Loader;
use Doctrine\Common\DataFixtures\Purger\ORMPurger;
use Doctrine\ORM\EntityManager;
use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;
use Symfony\Component\BrowserKit\Client;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\HttpKernel\Kernel;

class AbstractTest extends WebTestCase
{
    /**
     * @var Kernel
     */
    protected static $kernel;

    /**
     * @var ContainerInterface
     */
    protected static $container;

    /**
     * @var EntityManager
     */
    protected static $manager;

    /**
     * @var Client
     */
    protected $client;

    public static function setUpBeforeClass()
    {
        self::$kernel = new \App\Kernel('test', true);
        self::$kernel->boot();

        self::$container = self::$kernel->getContainer();
        self::$manager = self::$container->get('doctrine.orm.entity_manager');

        $loader = new Loader();
        $loader->addFixture(new FoodTruckFixture());

        $purger = new ORMPurger(self::$manager);
        $executor = new ORMExecutor(self::$manager, $purger);
        $executor->execute($loader->getFixtures());
    }
}
