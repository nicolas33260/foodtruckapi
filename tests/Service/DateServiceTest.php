<?php
/**
 * Created by PhpStorm.
 * User: Nicolas
 * Date: 2019-02-06
 * Time: 09:37.
 */

namespace App\Tests\Service;

use App\Service\DateService;
use PHPUnit\Framework\TestCase;

class DateServiceTest extends TestCase
{
    /**
     * @throws \Exception
     */
    public function testConvert()
    {
        $dateService = new DateService();

        $this->assertInstanceOf(\DateTime::class, $dateService->convert('02-02-2019'));
        $this->assertFalse($dateService->convert('02/02/2019'));
        $this->assertFalse($dateService->convert('32-02-19'));
    }
}
