<?php

namespace App\Tests\Entity;

use App\Entity\Booking;
use App\Entity\FoodTruck;
use ICanBoogie\Inflector;
use PHPUnit\Framework\TestCase;

class EntitiesTest extends TestCase
{
    /**
     * @var Inflector
     */
    private $inflector;

    /**
     * @throws \ReflectionException
     */
    public function testSetter()
    {
        $this->inflector = Inflector::get();

        $this->check(FoodTruck::class);
        $this->check(Booking::class);
    }

    /**
     * @param $class
     *
     * @throws \ReflectionException
     */
    private function check($class)
    {
        $reflectionClass = new \ReflectionClass($class);

        $suitablesReturnSetter = ['self'];

        foreach ($reflectionClass->getInterfaceNames() as $interfaceName) {
            $name = explode('/', $interfaceName);
            $suitablesReturnSetter[] = $name[count($name) - 1];
        }

        $properties = $reflectionClass->getProperties();
        $methods = $reflectionClass->getMethods();

        foreach ($properties as $property) {
            /** @var \ReflectionProperty */
            $camelCases = $this->inflector->camelize($property->getName());

            if (preg_match_all('/OneToMany/', $property->getDocComment())) {
                $singularize = $this->inflector->singularize($property->getName());

                $this->assertTrue(
                    $reflectionClass->hasMethod('add'.ucfirst($singularize)),
                    'Getter '.$class.'->'.$property->getName().' missing'
                );

                $this->assertTrue(
                    $reflectionClass->hasMethod('remove'.ucfirst($singularize)),
                    'Getter '.$class.'->'.$property->getName().' missing'
                );
            } else {
                if ('id' !== $property->getName()) {
                    $this->assertTrue(
                        $reflectionClass->hasMethod('set'.$camelCases),
                        'Setter '.$class.'->'.$property->getName().' missing'
                    );
                }
            }

            $this->assertTrue(
                $reflectionClass->hasMethod('get'.$camelCases),
                'Getter '.$class.'->'.$property->getName().' missing'
            );

            $this->assertFalse($property->isPublic(), 'Entity\'s property has to be private or protected');
        }

        foreach ($methods as $method) {
            /** @var \ReflectionMethod $method */
            if (preg_match('/set/', $method->name)) {
                $this->assertTrue(
                    in_array($method->getReturnType(), $suitablesReturnSetter),
                    $class.'->'.$method->name.' Please execute php bin/console make:entity --regenerate --overwrite'
                );
            }
        }
    }
}
