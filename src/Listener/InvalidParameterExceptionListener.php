<?php
/**
 * Created by PhpStorm.
 * User: Nicolas
 * Date: 2019-02-06
 * Time: 16:58.
 */

namespace App\Listener;

use FOS\RestBundle\Exception\InvalidParameterException;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Event\GetResponseForExceptionEvent;
use Symfony\Component\HttpKernel\Exception\HttpExceptionInterface;

class InvalidParameterExceptionListener
{
    /**
     * @param GetResponseForExceptionEvent $event
     */
    public function onKernelException(GetResponseForExceptionEvent $event)
    {
        $exception = $event->getException();

        if (!$exception instanceof InvalidParameterException) {
            return;
        }

        $issue = [
            'message' => $exception->getMessage(),
            'code' => $exception->getStatusCode(),
        ];

        $response = new JsonResponse($issue);

        if ($exception instanceof HttpExceptionInterface) {
            $response->setStatusCode($exception->getStatusCode());
            $response->headers->replace($exception->getHeaders());
        } else {
            $response->setStatusCode(Response::HTTP_INTERNAL_SERVER_ERROR);
        }

        $event->setResponse($response);
    }
}
