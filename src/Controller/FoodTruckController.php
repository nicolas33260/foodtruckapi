<?php
/**
 * Created by PhpStorm.
 * User: Nicolas
 * Date: 2019-02-05
 * Time: 17:55.
 */

namespace App\Controller;

use App\Entity\FoodTruck;
use App\Form\FoodTruckType;
use FOS\RestBundle\Controller\AbstractFOSRestController;
use FOS\RestBundle\Controller\Annotations as Rest;
use FOS\RestBundle\Request\ParamFetcher;
use Nelmio\ApiDocBundle\Annotation\Model;
use Swagger\Annotations as SWG;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;

/**
 * Class FoodTruckController.
 *
 * @Rest\Route("/api/v1")
 */
class FoodTruckController extends AbstractFOSRestController
{
    /**
     * @Rest\Get(name="foodtruck_list", path="/foodtrucks")
     *
     * @SWG\Tag(name="FoodTruck")
     *
     * @SWG\Response(
     *     response=200,
     *     description="Return the list of footrucks"
     * )
     *
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function listAction()
    {
        $foodTrucks = $this->repository()
            ->findAll();

        return $this->handleView(
            $this->view($foodTrucks)
        );
    }

    /**
     * @Rest\Post(name="food_truck_create", path="/foodtruck")
     * @Rest\RequestParam(name="foodTruck", nullable=false)
     *
     * @SWG\Tag(name="FoodTruck")
     *
     * @SWG\Response(
     *     response=200,
     *     description="Return the footruck just created"
     * )
     * @SWG\Response(
     *     response=500,
     *     description="Error in submitting data"
     * )
     *
     * @SWG\Parameter(
     *     name="foodTruck",
     *     required=true,
     *     description="Body of the foodTruck",
     *     in="body",
     *     @SWG\Schema(type="object",
     *         @SWG\Property(property="foodTruck", ref=@Model(type=FoodTruck::class))
     *     )
     * )
     *
     * @param ParamFetcher $fetcher
     *
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function createAction(ParamFetcher $fetcher)
    {
        $form = $this->createForm(FoodTruckType::class, new FoodTruck());
        $form->submit($fetcher->get('foodTruck'));

        if (false === $form->isValid()) {
            $errors = [];
            foreach ($form->getErrors(true) as $error) {
                $errors[$error->getCause()->getPropertyPath()] = $error->getMessage();
            }

            return $this->handleView(
                $this->view($errors, 500)
            );
        }

        $foodTruck = $form->getData();

        $manager = $this->getDoctrine()->getManager();
        $manager->persist($foodTruck);
        $manager->flush();

        return $this->handleView(
            $this->view($foodTruck, Response::HTTP_CREATED)
        );
    }

    /**
     * @Rest\Delete(name="food_truck_delete", path="/foodtruck/{id}")
     *
     * @SWG\Tag(name="FoodTruck")
     *
     * @SWG\Response(
     *     response=404,
     *     description="FoodTruck not found"
     * )
     * @SWG\Response(
     *     response=200,
     *     description="FoodTruck delete with success"
     * )
     *
     * @param $id
     *
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function deleteAction($id)
    {
        $foodTruck = $this->repository()
            ->find($id);

        if (!$foodTruck) {
            throw new NotFoundHttpException();
        }

        $manager = $this->getDoctrine()->getManager();
        $manager->remove($foodTruck);
        $manager->flush();

        return $this->handleView(
            $this->view('FoodTruck has been deleted')
        );
    }

    /**
     * @return \App\Repository\BookingRepository|\Doctrine\Common\Persistence\ObjectRepository
     */
    private function repository()
    {
        return $this->getDoctrine()
            ->getRepository(FoodTruck::class);
    }
}
