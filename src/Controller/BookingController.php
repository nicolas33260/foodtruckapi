<?php
/**
 * Created by PhpStorm.
 * User: Nicolas
 * Date: 2019-02-05
 * Time: 15:37.
 */

namespace App\Controller;

use App\Entity\Booking;
use App\Form\BookingType;
use App\Service\DateService;
use FOS\RestBundle\Controller\AbstractFOSRestController;
use FOS\RestBundle\Controller\Annotations as Rest;
use FOS\RestBundle\Request\ParamFetcher;
use Nelmio\ApiDocBundle\Annotation\Model;
use Swagger\Annotations as SWG;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;

/**
 * Class BookingController.
 *
 * @Rest\Route("/api/v1")
 */
class BookingController extends AbstractFOSRestController
{
    /** @var array */
    protected $bookingSpaces;

    /** @var int */
    private $bookingByWeek;

    /**
     * BookingController constructor.
     *
     * @param array $bookingSpaces
     * @param int   $bookingByWeek
     */
    public function __construct(array $bookingSpaces, int $bookingByWeek)
    {
        $this->bookingSpaces = $bookingSpaces;
        $this->bookingByWeek = $bookingByWeek;
    }

    /**
     * @Rest\Get(name="bookings_by_date", path="/bookings")
     * @Rest\QueryParam(
     *     name="date",
     *     nullable=true,
     *     strict=true,
     *     requirements="\d{2}\-\d{2}\-\d{4}"
     * )
     *
     * @SWG\Tag(name="Booking")
     * @SWG\Response(
     *     response=200,
     *     description="Return the list of the booking for a day"
     * )
     * @SWG\Response(
     *     response=500,
     *     description="If the date is not bookable"
     * )
     * @SWG\Parameter(
     *     name="date",
     *     required=false,
     *     format="dd-mm-yyyy",
     *     allowEmptyValue=false,
     *     in="query",
     *     type="string"
     * )
     *
     * @param ParamFetcher $fetcher
     * @param DateService  $dateService
     *
     * @return \Symfony\Component\HttpFoundation\Response
     *
     * @throws \Exception
     */
    public function listAction(ParamFetcher $fetcher, DateService $dateService)
    {
        $date = $dateService->convert($fetcher->get('date'));

        if (!$date) {
            return $this->handleView(
                $this->view('Date format must be d-m-Y and must be exist', 500)
            );
        }

        if (!isset($this->bookingSpaces[$dateService->getWeekDay()])) {
            return $this->handleView(
                $this->view('This day is not bookable', 500)
            );
        }

        $bookAvailable = $this->bookingSpaces[$dateService->getWeekDay()];

        $bookings = $this->repository()
            ->findBookings($date);

        $response = [
            'date' => $date,
            'bookingAvailableForThisDay' => $bookAvailable,
            'bookingStillAvailable' => $bookAvailable - count($bookings),
            'bookings' => $bookings,
        ];

        return $this->handleView(
            $this->view($response)
        );
    }

    /**
     * @Rest\Post(name="booking_create", path="/booking")
     * @Rest\RequestParam(name="booking", nullable=false)
     *
     * @SWG\Tag(name="Booking")
     * @SWG\Response(
     *     response=200,
     *     description="Return the booking just created"
     * )
     * @SWG\Response(
     *     response=500,
     *     description="Error in submitting data"
     * )
     * @SWG\Parameter(
     *     name="booking",
     *     required=true,
     *     description="Body of the booking",
     *     in="body",
     *     @SWG\Schema(type="object",
     *         @SWG\Property(property="booking", ref=@Model(type=Booking::class))
     *     )
     * )
     *
     * @param ParamFetcher $fetcher
     *
     * @return \Symfony\Component\HttpFoundation\Response
     *
     * @throws \Doctrine\ORM\NonUniqueResultException
     */
    public function createAction(ParamFetcher $fetcher)
    {
        $form = $this->createForm(BookingType::class, new Booking());
        $form->submit($fetcher->get('booking'));

        if (false === $form->isValid()) {
            $errors = [];
            foreach ($form->getErrors(true) as $error) {
                $errors[$error->getCause()->getPropertyPath()] = $error->getMessage();
            }

            return $this->handleView(
                $this->view($errors, 500)
            );
        }

        /** @var Booking $booking */
        $booking = $form->getData();

        $week = intval(
            strftime(
                '%U', strtotime($booking->getDate()->format('Y-m-d'))
            )
        );

        $countByWeek = $this->getDoctrine()
            ->getRepository(Booking::class)
            ->countWeekBookings($week, $booking->getFoodTruck());

        if ($countByWeek == $this->bookingByWeek) {
            return $this->handleView(
                $this->view("You can't book an emplacement more than 3 times by week", 500)
            );
        }

        $countBookings = $this->getDoctrine()
            ->getRepository(Booking::class)
            ->countBookings($booking->getDate());

        if (!isset($this->bookingSpaces[$booking->getDate()->format('D')])) {
            return $this->handleView(
                $this->view('This day is not bookable', 500)
            );
        }

        $bookAvailable = $this->bookingSpaces[
            $booking->getDate()->format('D')
        ];

        if ($countBookings == $bookAvailable) {
            return $this->handleView(
                $this->view('This day is full', 500)
            );
        }

        $manager = $this->getDoctrine()->getManager();
        $manager->persist($booking);
        $manager->flush();

        return $this->handleView(
            $this->view($booking, Response::HTTP_CREATED)
        );
    }

    /**
     * @Rest\Delete(name="booking_delete", path="/booking/{id}")
     *
     * @SWG\Tag(name="Booking")
     *
     * @SWG\Response(
     *     response=404,
     *     description="Booking not found"
     * )
     * @SWG\Response(
     *     response=200,
     *     description="Booking delete with success"
     * )
     *
     * @param $id
     *
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function deleteAction($id)
    {
        $booking = $this->repository()
            ->find($id);

        if (!$booking) {
            throw new NotFoundHttpException();
        }

        $manager = $this->getDoctrine()->getManager();
        $manager->remove($booking);
        $manager->flush();

        return $this->handleView(
            $this->view('Booking has been deleted')
        );
    }

    /**
     * @return \App\Repository\BookingRepository|\Doctrine\Common\Persistence\ObjectRepository
     */
    private function repository()
    {
        return $this->getDoctrine()
            ->getRepository(Booking::class);
    }
}
