<?php

namespace App\Form;

use App\Entity\Booking;
use App\Form\DataTransformer\FoodTruckToNumberTransformer;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\DateType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Validator\Constraints\Date;
use Symfony\Component\Validator\Constraints\GreaterThanOrEqual;
use Symfony\Component\Validator\Constraints\NotBlank;

class BookingType extends AbstractType
{
    /** @var FoodTruckToNumberTransformer */
    private $transformer;

    /**
     * BookingType constructor.
     *
     * @param FoodTruckToNumberTransformer $transformer
     */
    public function __construct(FoodTruckToNumberTransformer $transformer)
    {
        $this->transformer = $transformer;
    }

    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('date', DateType::class, [
                'widget' => 'single_text',
                'format' => 'dd-MM-yyyy',
                'invalid_message' => 'Date format should be dd-MM-yyyy',
                'constraints' => [
                    new NotBlank([
                        'message' => 'Required',
                    ]),
                    new Date(),
                    new GreaterThanOrEqual([
                        'value' => new \DateTime(date('d-m-Y')),
                        'message' => 'Please choose a date from today',
                    ]),
                ],
            ])
            ->add('foodTruck', TextType::class)
        ;

        $builder->get('foodTruck')
            ->addModelTransformer($this->transformer);
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => Booking::class,
        ]);
    }
}
