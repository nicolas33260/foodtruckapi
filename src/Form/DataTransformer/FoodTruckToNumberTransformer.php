<?php
/**
 * Created by PhpStorm.
 * User: Nicolas
 * Date: 2019-02-05
 * Time: 18:47.
 */

namespace App\Form\DataTransformer;

use App\Entity\FoodTruck;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\Form\DataTransformerInterface;
use Symfony\Component\Form\Exception\TransformationFailedException;

class FoodTruckToNumberTransformer implements DataTransformerInterface
{
    /** @var EntityManagerInterface */
    private $manager;

    /**
     * FoodTruckToNumberTransformer constructor.
     *
     * @param EntityManagerInterface $manager
     */
    public function __construct(EntityManagerInterface $manager)
    {
        $this->manager = $manager;
    }

    /**
     * @param mixed $foodTruck
     *
     * @return string
     */
    public function transform($foodTruck): string
    {
        if (null === $foodTruck) {
            return '';
        }

        return $foodTruck->getId();
    }

    /**
     * @param mixed $number
     *
     * @return FoodTruck|null
     */
    public function reverseTransform($number): ?FoodTruck
    {
        if (!$number) {
            return null;
        }

        $foodTruck = $this->manager
            ->getRepository(FoodTruck::class)
            ->find($number)
        ;

        if (null === $foodTruck) {
            throw new TransformationFailedException(sprintf(
                'An issue with number "%s" does not exist!',
                $number
            ));
        }

        return $foodTruck;
    }
}
