<?php

namespace App\Repository;

use App\Entity\Booking;
use App\Entity\FoodTruck;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Symfony\Bridge\Doctrine\RegistryInterface;

/**
 * @method Booking|null find($id, $lockMode = null, $lockVersion = null)
 * @method Booking|null findOneBy(array $criteria, array $orderBy = null)
 * @method Booking[]    findAll()
 * @method Booking[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class BookingRepository extends ServiceEntityRepository
{
    public function __construct(RegistryInterface $registry)
    {
        parent::__construct($registry, Booking::class);
    }

    /**
     * @param \DateTimeInterface $dateTime
     *
     * @return array
     */
    public function findBookings(\DateTimeInterface $dateTime): array
    {
        return $this->createQueryBuilder('b')
            ->andWhere('b.date = :date')
            ->setParameter('date', $dateTime->format('Y-m-d'))
            ->getQuery()
            ->getResult()
        ;
    }

    /**
     * @param \DateTimeInterface $dateTime
     *
     * @return int
     *
     * @throws \Doctrine\ORM\NonUniqueResultException
     */
    public function countBookings(\DateTimeInterface $dateTime): int
    {
        return $this->createQueryBuilder('b')
            ->select('COUNT(b)')
            ->andWhere('b.date = :date')
            ->setParameter('date', $dateTime->format('Y-m-d'))
            ->getQuery()
            ->getSingleScalarResult()
            ;
    }

    /**
     * @param int       $week
     * @param FoodTruck $foodTruck
     *
     * @return mixed
     *
     * @throws \Doctrine\ORM\NonUniqueResultException
     */
    public function countWeekBookings(int $week, FoodTruck $foodTruck)
    {
        return $this->createQueryBuilder('b')
            ->select('COUNT(b)')
            ->andWhere('b.foodTruck = :foodTruck')
            ->andWhere('WEEK(b.date) = :week')
            ->setParameter('foodTruck', $foodTruck)
            ->setParameter('week', $week)
            ->getQuery()
            ->getSingleScalarResult()
            ;
    }
}
