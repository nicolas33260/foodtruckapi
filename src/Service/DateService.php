<?php
/**
 * Created by PhpStorm.
 * User: Nicolas
 * Date: 2019-02-05
 * Time: 17:01.
 */

namespace App\Service;

class DateService
{
    /** @var \DateTime */
    protected $date;

    /**
     * @param string|null $date
     *
     * @return \DateTime|bool
     *
     * @throws \Exception
     */
    public function convert(string $date = null)
    {
        if ($date && !preg_match('#\d{2}\-\d{2}\-\d{4}#', $date)) {
            return false;
        }

        $this->date = $date ? \DateTime::createFromFormat('d-m-Y', $date) : new \DateTime();

        return $this->date;
    }

    /**
     * @return string|null
     */
    public function getWeekDay(): ?string
    {
        return $this->date ? $this->date->format('D') : null;
    }
}
