<?php
/**
 * Created by PhpStorm.
 * User: Nicolas
 * Date: 2019-02-06
 * Time: 13:43.
 */

namespace App\Fixtures;

use App\Entity\FoodTruck;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\Persistence\ObjectManager;

class FoodTruckFixture extends Fixture
{
    /**
     * @param ObjectManager $manager
     *
     * @throws \Exception
     */
    public function load(ObjectManager $manager)
    {
        $specialties = [
            'Japonais', 'Italien', 'Espagnol', 'Portugais',
            'Chinois', 'Italien', 'Japonais', 'Burger', 'Sandwich',
        ];

        $index = 1;

        foreach ($specialties as $specialty) {
            $truck = new FoodTruck();
            $truck->setName('Foo'.$index)
                ->setSpecialty($specialty);

            $manager->persist($truck);
            ++$index;
        }

        $manager->flush();
    }
}
