<?php

use Doctrine\ORM\Tools\Console\ConsoleRunner;
use Doctrine\ORM\Tools\Setup;
use Doctrine\ORM\EntityManager;

// replace with file to your own project bootstrap
require_once 'vendor/autoload.php';

// replace with mechanism to retrieve EntityManager in your app
//$entityManager = GetEntityManager();

$paths = array('src/Entity');
$isDevMode = true;

// the connection configuration
$dbParams = array(
    'host' => 'mysql',
    'driver' => 'pdo_mysql',
    'user' => 'root',
    'password' => 'rockmyroot',
    'dbname' => 'davidson_rest',
);

$config = Setup::createAnnotationMetadataConfiguration($paths, $isDevMode);
$entityManager = EntityManager::create($dbParams, $config);

return ConsoleRunner::createHelperSet($entityManager);
