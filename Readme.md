FoodTruck Rest Api
==================

### Installation

````bash
composer install
````

### Documentation de l'API

La documentation a été généré par le Swagger de nelmio
<http://foodtruck.nips.fr/api/doc>

### Tester l'API

````bash
./vendor/bin/grumphp run 
````

ou

````bash
./vendor/bin/phpunit 
````

###CDC

* Il y a 8 emplacements du lundi au jeudi et 7 le vendredi.
* Un foodtruck ne peut reserver qu'un seul emplacement par jour.
* Un foodtruck ne peut reserver que 3 emplacements par semaine.
* Aucune réservation n'est possible le Samedi et le Dimanche.